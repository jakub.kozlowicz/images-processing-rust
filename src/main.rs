use std::fs::File;
use std::io;

use images_processing::image::{pgm::PGMImage, ppm::PPMImage};

fn main() -> Result<(), io::Error> {
    let pgm_input_file = File::open("/Users/jakub/Projects/images_processing/images/kubus.pgm")?;
    let pgm_image = PGMImage::new(pgm_input_file)?;
    let mut pgm_output_file =
        File::create("/Users/jakub/Projects/images_processing/images/kubus2.pgm")?;
    pgm_image.save_to_file(&mut pgm_output_file)?;

    let mut ppm_input_file =
        File::open("/Users/jakub/Projects/images_processing/images/kubus.ppm")?;
    let ppm_image = PPMImage::new(&mut ppm_input_file)?;
    let mut ppm_output_file =
        File::create("/Users/jakub/Projects/images_processing/images/kubus2.ppm")?;
    ppm_image.save_to_file(&mut ppm_output_file)?;

    Ok(())
}
