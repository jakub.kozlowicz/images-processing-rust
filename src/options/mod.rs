struct ImageOptions {
    input_image: String,
    output_image: String,
    conversion: Option<bool>,
    threshold: Option<u8>,
    white_threshold: Option<u8>,
    black_threshold: Option<u8>,
    negative: Option<bool>,
    contouring: Option<bool>,
    histogram: Option<bool>,
    horizontal_blur: Option<bool>,
    vertical_blur: Option<bool>,
}
