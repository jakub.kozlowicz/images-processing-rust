use std::{
    fs::File,
    io::{self, BufRead, Read, Write},
};

use ndarray::Array2;

#[derive(Debug)]
pub enum PPMColorToProcess {
    RED,
    GREEN,
    BLUE,
}

#[derive(Debug)]
pub struct PPMImage {
    x_size: usize,
    y_size: usize,
    color_scale: usize,
    r_pixels: Array2<usize>,
    g_pixels: Array2<usize>,
    b_pixels: Array2<usize>,
}

impl PPMImage {
    pub fn new(file: &mut File) -> Result<Self, io::Error> {
        let mut file_content = String::new();
        file.read_to_string(&mut file_content)?;
        let mut cursor = io::Cursor::new(file_content);
        let mut line = String::new();

        // Read magic number
        cursor.read_line(&mut line)?;
        if line != "P3" {
            return Err(io::Error::new(
                io::ErrorKind::InvalidData,
                "Mismatched magic number",
            ));
        }
        line.clear();

        // Skip comments
        cursor.read_line(&mut line)?;
        while line.starts_with('#') {
            line.clear();
            cursor.read_line(&mut line)?;
        }

        // Read image dimensions
        // Do not read line again because of while loop
        let x_size: usize;
        let y_size: usize;
        let mut dimensions = line.split_whitespace().map(|s| s.parse::<usize>());
        if let (Some(Ok(x)), Some(Ok(y))) = (dimensions.next(), dimensions.next()) {
            x_size = x;
            y_size = y;
        } else {
            x_size = 0;
            y_size = 0;
        }
        line.clear();

        // Read color scale
        cursor.read_line(&mut line)?;
        let color_scale = line.parse::<usize>().unwrap();
        line.clear();

        // Read individual pixels
        let mut pixels = String::new();
        cursor.read_to_string(&mut pixels)?;
        let pixels: Vec<usize> = pixels
            .split_whitespace()
            .map(|s| s.parse::<usize>().unwrap())
            .collect();
        let mut pixels = pixels.iter();

        let mut r_pixels: ndarray::Array2<usize> = Array2::zeros((x_size, y_size));
        let mut g_pixels: ndarray::Array2<usize> = Array2::zeros((x_size, y_size));
        let mut b_pixels: ndarray::Array2<usize> = Array2::zeros((x_size, y_size));

        for i in 0..x_size {
            for j in 0..y_size {
                r_pixels[[i, j]] = pixels.next().unwrap().clone();
                g_pixels[[i, j]] = pixels.next().unwrap().clone();
                b_pixels[[i, j]] = pixels.next().unwrap().clone();
            }
        }

        Ok(Self {
            x_size,
            y_size,
            color_scale,
            r_pixels,
            g_pixels,
            b_pixels,
        })
    }

    pub fn save_to_file(&self, file: &mut File) -> Result<(), io::Error> {
        let mut content = String::new();

        content.push_str("P3\n");
        content.push_str(&format!(
            "{} {}\r{}\n",
            self.x_size, self.y_size, self.color_scale
        ));

        for i in 0..self.x_size {
            for j in 0..self.y_size {
                content.push_str(&format!(
                    "{} {} {} ",
                    self.r_pixels[[i, j]],
                    self.g_pixels[[i, j]],
                    self.b_pixels[[i, j]]
                ));
            }
            content.push_str("\n");
        }

        file.write_all(content.as_bytes())?;

        Ok(())
    }
}
