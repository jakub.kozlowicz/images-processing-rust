use std::{
    fs::File,
    io::{self, BufRead, BufReader, Write},
};

use ndarray::Array2;

#[derive(Debug)]
pub struct PGMImage {
    x_size: usize,
    y_size: usize,
    color_scale: usize,
    pixels: Array2<usize>,
}

impl PGMImage {
    pub fn new(file: File) -> Result<Self, io::Error> {
        let reader = BufReader::new(file);
        let mut lines_iter = reader.lines();

        // Read magic number
        let mut line = lines_iter.next().unwrap().unwrap();
        if line != "P2" {
            panic!("Error reading magic number");
        }

        // Skip comments
        line = lines_iter.next().unwrap().unwrap();
        while line.starts_with("#") {
            line = lines_iter.next().unwrap().unwrap();
        }

        // Read image dimensions
        // Do not call next because of next in while loop
        let x_size: usize;
        let y_size: usize;
        let mut dimensions = line.split_whitespace().map(|s| s.parse::<usize>());
        match (dimensions.next(), dimensions.next()) {
            (Some(Ok(x)), Some(Ok(y))) => (x_size, y_size) = (x, y),
            _ => panic!("Error reading dimensions of the image"),
        }

        // Read colorscale
        line = lines_iter.next().unwrap().unwrap();
        let color_scale = line.parse().unwrap();

        // Read individual pixels
        let pix: Vec<String> = lines_iter.map(|l| l.unwrap()).collect();
        let pixs = pix.join(" ");
        let mut pixs = pixs.split_whitespace();

        let mut pixels = Array2::zeros((x_size, y_size));
        for i in 0..x_size {
            for j in 0..y_size {
                pixels[[i, j]] = pixs.next().unwrap().parse::<usize>().unwrap();
            }
        }

        Ok(Self {
            x_size,
            y_size,
            color_scale,
            pixels,
        })
    }

    pub fn save_to_file(&self, file: &mut File) -> Result<(), io::Error> {
        file.write(b"P2\n")?;
        file.write(format!("{} {}\r{}\n", self.x_size, self.y_size, self.color_scale).as_bytes())?;

        for i in 0..self.x_size {
            for j in 0..self.y_size {
                file.write(format!("{} ", self.pixels[[i, j]]).as_bytes())?;
            }
            file.write(b"\n")?;
        }

        Ok(())
    }
}
