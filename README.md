# PPM and PGM Images processing

Images processing of PPM and PGM images. This project is a rewrite of the [C
version](https://gitlab.com/jakub.kozlowicz/images-processing) in Rust.
